package routes

import (
	"cts-job-search/controllers"

	"github.com/gin-gonic/gin"
)

func AddRoutes(router *gin.RouterGroup) {
	api := router.Group("/api")
	{
		jobs := api.Group("/jobs")
		{
			jobs.POST("/add", controllers.AddJobsOnCloud)
			jobs.GET("/list",controllers.ListAllJobsFromCloud)
			jobs.GET("/search",controllers.SearchJobFilter)
		}
	}
}

func SetupRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode("debug")
	AddRoutes(&router.RouterGroup)
	return router
}
