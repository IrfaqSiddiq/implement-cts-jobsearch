package controllers

import (
	"context"
	"cts-job-search/models"
	"fmt"
	"log"
	"net/http"

	talentpb "cloud.google.com/go/talent/apiv4beta1/talentpb"
	"google.golang.org/api/iterator"

	"github.com/gin-gonic/gin"
)

// get latest value of talentpb from CTS documentation.
func AddJobsOnCloud(c *gin.Context) {
	ctx := context.Background()
	projectID, jobClient, err := GetJobServiceClient(c)
	if err != nil {
		log.Printf("AddJobsOnCloud: Failed to create job client: %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "failed while creating new job service",
		})
		return
	}
	defer jobClient.Close()
	job := &talentpb.Job{
		Company:       fmt.Sprintf("projects/%s/companies/%s", projectID, "amazon"),
		RequisitionId: "123456",
		Title:         "Amazon Delivery Driver",
		Description:   "We are looking for a delivery driver.",
	}
	req := &talentpb.CreateJobRequest{
		Parent: fmt.Sprintf("projects/%s", projectID),
		Job:    job,
	}
	resp, err := jobClient.CreateJob(ctx, req)
	if err != nil {
		log.Printf("Failed to create job on cloud: %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "failed while creating job on cloud",
		})
		return
	}
	//resp.GetName()=projects/{projectID}/jobs/{jobID}
	c.JSON(http.StatusOK, gin.H{
		"message": "successfully created" + resp.GetName(),
	})
}

func ListAllJobsFromCloud(c *gin.Context) {
	var ctsJobs []models.CTSJobs
	ctx := context.Background()
	projectID, jobClient, err := GetJobServiceClient(c)
	if err != nil {
		log.Printf("AddJobsOnCloud: Failed to create job client: %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "failed while creating new job service",
		})
		return
	}
	defer jobClient.Close()

	// Construct the list jobs request.
	req := &talentpb.ListJobsRequest{
		Parent: fmt.Sprintf("projects/%s", projectID),
	}

	// Execute the list jobs request.
	it := jobClient.ListJobs(ctx, req)
	if err != nil {
		fmt.Printf("Failed to list jobs: %v\n", err)
		return
	}
	for {
		job, err := it.Next()
		if err == iterator.Done {
			fmt.Printf("Done.\n")
			break
		}
		fmt.Printf("Job name: %s\n", job.GetName())
		fmt.Printf("Company name: %s\n", job.GetCompany())
		fmt.Printf("Requisition ID: %s\n", job.GetRequisitionId())
		fmt.Printf("Title: %s\n", job.GetTitle())
		fmt.Printf("Description: %s\n", job.GetDescription())
		fmt.Printf("Posting publish time: %v\n", job.GetPostingPublishTime().GetSeconds())
		fmt.Printf("Posting end time: %v\n", job.GetPostingExpireTime().GetSeconds())
		fmt.Printf("Job posting URL: %s\n", job.ApplicationInfo.Uris[0])
		ctsJobs = append(ctsJobs, models.CTSJobs{
			JobTitle:    job.GetTitle(),
			Description: job.GetDescription(),
			JobID:       job.GetRequisitionId(),
			JobURL:      job.ApplicationInfo.Uris[0],
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"job_list": ctsJobs,
	})
}

func SearchJobFilter(c *gin.Context) {
	var ctsJobs []models.CTSJobs
	ctx := context.Background()
	projectID, jobClient, err := GetJobServiceClient(c)
	if err != nil {
		log.Printf("AddJobsOnCloud: Failed to create job client: %v\n", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": "failed while creating new job service",
		})
		return
	}
	defer jobClient.Close()

	req := &talentpb.SearchJobsRequest{
		Parent: fmt.Sprintf("projects/%s", projectID),
		JobQuery: &talentpb.JobQuery{
			Query: "Amazon Warehouse",
		},
	}
	// Execute the list jobs request.
	resp, err := jobClient.SearchJobs(ctx, req)
	if err != nil {
		fmt.Printf("Failed to list jobs: %v\n", err)
		return
	}
	for _, job := range resp.GetMatchingJobs() {
		fmt.Printf("Job name: %s\n", job.Job.GetTitle())
		fmt.Printf("Company name: %s\n", job.Job.GetCompany())
		fmt.Printf("Requisition ID: %s\n", job.Job.RequisitionId)
		fmt.Printf("Title: %s\n", job.Job.GetTitle())
		fmt.Printf("Description: %s\n", job.Job.Description)
		fmt.Printf("Job posting URL: %s\n", job.Job.ApplicationInfo.Uris[0])
		fmt.Println("------------------------------------")
		ctsJobs = append(ctsJobs, models.CTSJobs{
			JobTitle:    job.Job.Title,
			Description: job.Job.Description,
			JobID:       job.Job.RequisitionId,
			JobURL:      job.Job.ApplicationInfo.Uris[0],
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"job_list": ctsJobs,
	})
}
